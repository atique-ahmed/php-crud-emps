
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<div class="container">
<form action="add_action.php" style="margin-top:30px;">
  <div class="mb-3">
    <input placeholder="Name" class="form-control" name="name">
  </div>
 <div class="mb-3">
    <input placeholder="Contact Number" class="form-control" name="contact_number">
  </div>
 <div class="mb-3">
    <input placeholder="Address" class="form-control" name="address">
  </div>
 <div class="mb-3">
    <input placeholder="Salary" class="form-control" name="salary">
  </div>
 <div class="mb-3">
    <input placeholder="Employee Id" class="form-control" name="employee_id">
  </div>

 <div class="mb-3">
    <input placeholder="Role" class="form-control" name="role">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
